require 'spec_helper'

describe "Api::V1::AdditionalsController" do
  describe 'GET #user (integrated)', :type => :request do
    let!(:application) { Doorkeeper::Application.create!(name: 'MyApp', redirect_uri: 'https://app.com') }
    let!(:user) { User.create!(name: "abc", email: 'abc@b.com', password: 'abc123', password_confirmation: 'abc123') }
    let!(:token) { Doorkeeper::AccessToken.create!(application_id: application.id, resource_owner_id: user.id, scopes: 'public') }

    it 'responds with 200' do
      get api_v1_user_path, headers: {
          'Authorization' => "Bearer #{token.token}"
      }
      expect(response.status).to eq(200)
    end
    it 'returns the user as json' do
      get api_v1_user_url, headers: {
          'Authorization' => "Bearer #{token.token}"
      }
      expect(response.body).to eq(user.to_json)
    end
  end
end